<?php

use Faker\Generator as Faker;

$factory->define(App\Shipping::class, function (Faker $faker) {
    return [
        'address' => $faker->address,
        'deliver_at' => $faker->dateTimeBetween('+1 day', '+7 days'),
        'time' => $faker->randomElement(['1h', '2h','3h','4h','5h','6h','7h', '8h']),
        'observations' => $faker->sentence
    ];
});
