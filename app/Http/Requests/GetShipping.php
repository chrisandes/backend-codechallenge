<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetShipping extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'start' => 'required:date,date_format:Y-m-d',
            'end' => 'required:date,date_format:Y-m-'
            
        ];
    }
}
